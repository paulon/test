//
//  main.m
//  Controller&Navigation
//
//  Created by Paulo Rodrigo Noronha Orozco on 17/07/17.
//  Copyright © 2017 Paulo Rodrigo Noronha Orozco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
