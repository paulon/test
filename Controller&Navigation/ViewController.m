//
//  ViewController.m
//  Controller&Navigation
//
//  Created by Paulo Rodrigo Noronha Orozco on 17/07/17.
//  Copyright © 2017 Paulo Rodrigo Noronha Orozco. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
